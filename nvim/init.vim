" map the leader key to comma
let g:mapleader=","

" plugins
call plug#begin('~/.config/nvim/plugged')

Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'dp': './install --all' }
Plug 'junegunn/fzf.vim'
function! DoRemote(arg)
    UpdateRemotePlugins
endfunction
Plug 'Shougo/deoplete.nvim', { 'do' : function('DoRemote') }
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'Raimondi/delimitMate'

call plug#end()

" theme
set termguicolors
set background=dark
colorscheme deep-space

" deep space
let g:deepspace_italics=1

" airline
let g:airline_theme='deep_space'
let g:airline_powerline_fonts=1
let g:airline#extensions#bufferline#enabled=1

" deoplete
let g:deoplete#enable_at_startup=1
set completeopt+=noinsert

" delimitMate
let g:delimitMate_expand_cr=1
let g:delimitMate_expand_space=1
let g:delimitMate_balance_matchpairs=1

" syntax highlighting on
syntax on

" visual editor settings
set showmatch
set cursorline
set number
set relativenumber
set fillchars+=vert:│

" disable hidden buffers
set nohidden

" line wrapping
set wrap
set linebreak
set textwidth=80
set wrapmargin=0
set formatoptions+=t

" don't show the status (airline shows it)
set noshowmode

" show two status lines
set laststatus=2

" set custom line wrap character
set showbreak=↳

" read external changes
set autoread

" leave 5 lines of overlap when scrolling
set scrolloff=5
set sidescrolloff=5

" disable mouse
set mouse=
set mousehide

" better window splitting
set splitright
set splitbelow

" better backspacing and paste behaviour
set backspace=indent,eol,start
set pastetoggle=<F12>

" searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" indentation
filetype plugin indent on
set autoindent
set smartindent
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

" longer undo history
set undolevels=1000

" set directories for backup, swp and undo files
set backup
set writebackup
set backupdir=~/.vim/backup//,/var/tmp//,/tmp//
set directory=~/.vim/swap//,/var/tmp//,/tmp//
if has('persistent_undo')
    set undofile
    set undodir=~/.vim/undo//,/var/tmp//,/tmp//
endif

" unbind the arrow keys
for prefix in ['i', 'n', 'v']
    for key in ['<Up>', '<Down>', '<Left>', '<Right>']
        exe prefix . "noremap" . key . " <Nop>"
    endfor
endfor

" keybindings
inoremap jk <esc>
inoremap <esc> <nop>
vnoremap v <esc>
vnoremap <esc> <nop>
tnoremap jk <C-\><C-n>

nnoremap <silent> <leader>q :NERDTreeToggle<CR>

nnoremap <silent> <leader>a :Files<CR>
nnoremap <silent> <leader>s :BLines<CR>
nnoremap <silent> <leader>d :Lines<CR>

nnoremap <leader>ev :vsplit $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

vnoremap <leader>" <esc>`<i"<esc>`>a"<esc>

nnoremap H _
nnoremap L $

nnoremap - :m -2<CR>
nnoremap + :m +1<CR>

nnoremap > >>
nnoremap < <<
vnoremap > >gv
vnoremap < <gv

nnoremap <leader>hh <c-w>h
nnoremap <leader>ll <c-w>l
nnoremap <leader>kk <c-w>k
nnoremap <leader>jj <c-w>j
nnoremap <leader>HH <c-w>H
nnoremap <leader>LL <c-w>L
nnoremap <leader>KK <c-w>K
nnoremap <leader>JJ <c-w>J
nnoremap <leader>hn :leftabove vnew<CR>
nnoremap <leader>ln :rightbelow vnew<CR>
nnoremap <leader>kn :leftabove new<CR>
nnoremap <leader>jn :rightbelow new<CR>
nnoremap <silent> <leader>h<space> :leftabove vsplit<CR>:Files<CR>
nnoremap <silent> <leader>l<space> :rightbelow vsplit<CR>:Files<CR>
nnoremap <silent> <leader>k<space> :leftabove split<CR>:Files<CR>
nnoremap <silent> <leader>j<space> :rightbelow split<CR>:Files<CR>
nnoremap <silent> <leader>ht :leftabove vnew<CR>:call termopen('fish')<CR>i
nnoremap <silent> <leader>lt :rightbelow vnew<CR>:call termopen('fish')<CR>i
nnoremap <silent> <leader>kt :leftabove new<CR>:call termopen('fish')<CR>i
nnoremap <silent> <leader>jt :rightbelow new<CR>:call termopen('fish')<CR>i

imap <expr> <CR> pumvisible() ? "\<c-y>" : "\<Plug>delimitMateCR"
inoremap <expr> <tab> pumvisible() ? "\<c-y>" : "\<tab>"
inoremap <expr> . pumvisible() ? "\<c-y>." : "\."
inoremap <expr> ; pumvisible() ? "\<c-y>;" : "\;"
